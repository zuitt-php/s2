<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity 2</title>
</head>
<body>

<h1>Numbers Divisible by 5</h1>
<hr/>

<?php divisibleBy5() ?>

<br/><br/><br/>

<h1>Array Manipulation</h1>
<hr/>

<?php array_push($students, 'Chaeyoung Son'); ?>
<p><?php var_dump($students); ?></p>
<p>Number of Students: <?= count($students) ?></p>
<?php array_push($students, 'Kim Dahyun'); ?>
<p><?php var_dump($students); ?></p>
<p>Number of Students: <?= count($students) ?></p>
<?php array_shift($students); ?>
<p><?php var_dump($students); ?></p>
<p>Number of Students: <?= count($students) ?></p>

</body>
</html>