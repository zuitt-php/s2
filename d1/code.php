<?php

//Repetition Control Structures

/*
While Loop
- takes a single condition, for as long as the condition is true, the code inside the block will run
*/

function whileLoop(){
	$count = 0;
	while($count <= 5){
		echo $count . '<br/>';
		$count++;
	}
}

/*
Do-While Loop
- run at least once, before conditions are checked.
*/

function doWhileLoop(){
	$count = 20;
	do{
		echo $count . '<br/>';
		$count--;
	}while($count > 0);
}

/*
For Loop
- flexible kind of loop. Consists of three parts:
- The initial value that will track the progression of the loop
- The condition that will be evaluated and will determine whether the loop will continue running or not
- The iteration method that indicates how to advance the loop
*/

function forLoop(){
	for($count = 0; $count <= 10; $count++){
		echo $count . "<br/>";
	}
}

//Array Manipulation

//Simple Array
$studentNumbers = array('1923', '1924', '1925', '1926');
//$studentNumbers = ['1923', '1924', '1925', '1926'];
$grades = [98.5, 94.3, 89.2, 90.1];

//Associative Arrays
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

//Multi-Dimensional Arrays
$heroes = [
	[ 'Iron Man', 'Thor', 'Hulk'],
	[ 'Wolverine', 'Cyclops', 'Jean Grey'],
	[ 'Batman', 'Superman', 'Wonder Woman']
];

//Multi-Dimensional Associative Arrays

$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Punch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching Arrays
function searchBrand($brandsArr, $brand){
	if(in_array($brand, $brandsArr)){
		return "$brand is in the array.";
	}else{
		return "$brand is NOT in the array.";
	}
}